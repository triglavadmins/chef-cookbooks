node[:deploy].each do |application, deploy|
  link "#{deploy[:deploy_to]}/current/.env" do
    to "#{deploy[:deploy_to]}/shared/.env"
  end
end