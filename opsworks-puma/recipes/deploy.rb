node[:deploy].each do |application, deploy|
  opsworks_deploy_user do
    deploy_data deploy
  end
  
  opsworks_deploy_dir do
    user deploy[:user]
    group deploy[:group]
    path deploy[:deploy_to]
  end

  puma_web_app do
    application application
    deploy deploy
  end

  Chef::Log.info("Creating dotenv symb_link")
  link "#{deploy[:deploy_to]}/current/.env" do
    to "#{deploy[:deploy_to]}/shared/.env"
  end

  service "youragora" do
    Chef::Log.info("Restarting Puma ...")
    action :restart
  end
end