execute "gem uninstall puma" do
  only_if "gem list | grep puma"
end

include_recipe "nginx"

directory "/etc/nginx/shared"
directory "/etc/nginx/http"
directory "/etc/nginx/ssl"

node[:deploy].each do |application, deploy|
  puma_config application do
    directory deploy[:deploy_to]
    environment deploy[:rails_env]
    logrotate false
    thread_min 0
    thread_max 16
    workers 2
    worker_timeout 60
    restart_timeout 120
    exec_prefix "bundle exec"
    prune_bundler false
  end
end