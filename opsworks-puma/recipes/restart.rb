node[:deploy].each do |application, deploy|
  service "youragora" do
    Chef::Log.info("Restarting Puma ...")
    action :restart
  end
end